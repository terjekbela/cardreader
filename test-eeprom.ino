////////////////////////////////////////////////////////////////////////////////
// Developpe Cardreader - EEPROM read/write test sketch
////////////////////////////////////////////////////////////////////////////////

#include <AH_24Cxx.h>
#include <Wire.h> 
#define AT24C256 9
#define BUSADDRESS  0x00
#define EEPROMSIZE  32768

int data;
AH_24Cxx ic_eeprom = AH_24Cxx(AT24C256, BUSADDRESS);

void setup(){
  Serial.begin(115200);
  Wire.begin();
  Serial.println("Write data");
  for(int i = 0; i < 20; i++) {
    data = i;
    ic_eeprom.write_byte(i,data);
    delay(200);
  }
}

void loop(){
  Serial.println("Read data");
  for (int i=0;i<EEPROMSIZE;i++){
    Serial.print("pos. ");
    Serial.print(i);
    Serial.print(": ");
    Serial.println(ic_eeprom.read_byte(i));
    delay(1000);
  }
}
