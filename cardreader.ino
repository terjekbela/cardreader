////////////////////////////////////////////////////////////////////////////////
// Developpe Cardreader
////////////////////////////////////////////////////////////////////////////////
//
//
//
// Extra libraries used
// -----------------------------------------------------------------------------
// You need to install these libraries into you sketchbook folder
// in order to this sketch to compile and work properly
// - UIPEthernet       - uIP Ethernet library
// - Adafruit_PN532    - Adafruit PN532 NFC/RFID reader
// - LiquidCrystal_I2C - Modified LCD library for I2C 
//
// Arduino Nano connection diagram
// -----------------------------------------------------------------------------
//                ------------------
//               -| TX         VIN |- 9V  external power
//               -| RX         GND |- GND external power
//        BUTTON -| RESET    RESET |- uIP, PN532 RESET
//       uIP GND -| GND        5V+ |- uIP, PN532, LCD VCC
//       uIP IRQ -| D2        ADC7 |- 
//     LCD BLUE  -| D3        ADC6 |- 
//     PN532 IRQ -| D4        ADC5 |- PN532,EEPROM I2C SCL
//     LCD GREEN -| D5        ADC4 |- PN532,EEPROM I2C SDA
//     LCD RED   -| D6        ADC3 |- (LCD D4) - only for parallel lcds
//     (LCD RS)  -| D7        ADC2 |- (LCD D5)
//     (LCD EN)  -| D8        ADC1 |- (LCD D6)
//  PIEZO BUZZER -| D9        ADC0 |- (LCD D7)
//    uIP SPI CS -| D10       AREF |- 
//    uIP SPI SI -| D11        3V3 |- uIP VCC
//    uIP SPI SO -| D12        D13 |- uIP SPI SCK
//                ------------------
//
// Internal EEPROM map - stores configuration data
// -----------------------------------------------------------------------------
// 0x0000 -   2 bytes - version major/minor on lsb/msb
// 0x0002 -   6 bytes - cardreader mac address
// 0x0008 -   4 bytes - cardreader ip address
// 0x000c -   4 bytes - cardreader dns server ip address
// 0x0010 -   4 bytes - cardreader gateway ip address
// 0x0014 -   4 bytes - remote server ip address
// 0x0018 -   8 bytes - blank
// 0x0020 -  32 bytes - remote server name
// 0x0040 -  32 bytes - remote server url base
// 0x0060 -  32 bytes - remote server url for time download
// 0x0080 -  32 bytes - remote server url for card db download
// 0x00a0 -  32 bytes - remote server url for card list upload
// 0x00c0 -  64 bytes - lcd font definitions (8 bytes per char, 8 chars)
// 0x0100 - 256 bytes - blank
// 0x0200 - 256 bytes - blank
// 0x0300 - 256 bytes - blank
//
// External EEPROM map - stores(caches) NFC tag ids read and card database
// -----------------------------------------------------------------------------
// 0x0000 -   2 bytes - number of ids stored (8 bytes per tag id)
// 0x0002 -   6 bytes - blank
// 0x0008 -   8 bytes - tag id 0
// 0x0010 -   8 bytes - tag id 1
// ...
// 0x07f8 -   8 bytes - tag id n
//
// 0x0800 -   2 bytes - number of db entries stored (32 bytes per entry)
// 0x0802 -  30 bytes - blank
// 0x0820 -  32 bytes - entry 0
// 0x0840 -  32 bytes - entry 0
// ...
// 0xffe0 -  32 bytes - entry n
//
//
//
//
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
// Libraries used
////////////////////////////////////////////////////////////////////////////////

#include <Wire.h>                 // Arduino standard I2C library
#include <SPI.h>                  // Arduino standard SPI lib

//#include <LiquidCrystal.h>      // Arduino standard (parallel) LCD
#include <LiquidCrystal_I2C.h>    // Modified LCD library for I2C

#include <EEPROM.h>               // Arduino standard EEPROM library
#define EEPROM_INT_VER_MAJ  0x00  // major version
#define EEPROM_INT_VER_MIN  0x01  // minor version
#define EEPROM_INT_MAC      0x02  // mac address
#define EEPROM_INT_IP       0x08  // host ip
#define EEPROM_INT_DNS      0x0c  // dns server
#define EEPROM_INT_GW       0x10  // gateway
#define EEPROM_INT_SV       0x14  // remote server
#define EEPROM_INT_FONT     0xc0  // lcd font

#include <AH_24Cxx.h>             // Custom EEPROM library
#define EEPROM_EXT_AT24C01  0 
#define EEPROM_EXT_AT24C02  1
#define EEPROM_EXT_AT24C04  2
#define EEPROM_EXT_AT24C08  3
#define EEPROM_EXT_AT24C16  4
#define EEPROM_EXT_AT24C32  5
#define EEPROM_EXT_AT24C64  6
#define EEPROM_EXT_AT24C128 8
#define EEPROM_EXT_AT24C256 9
#define EEPROM_EXT_BUS      0x00  // 3 LSB bits of I2C address
#define EEPROM_EXT_SIZE     2048  // AT24C16 2048byte

//#include <Ethernet.h>           // Arduino standard Ethernet lib
#include <UIPEthernet.h>          // uIP Ethernet library
#undef UIPETHERNET_DEBUG_CLIENT   // https://github.com/ntruchsess/arduino_uip

#include <Adafruit_PN532.h>       // Adafruit PN532 NFC/RFID reader
#undef PN532DEBUG                 // https://github.com/adafruit/Adafruit-PN532/
#undef MIFAREDEBUG

//#include <SHT2x.h>              // Temp and Humidity sensor library
//#define SHT2x_DEBUG == 0        // https://github.com/misenso/SHT2x-Arduino-Library

//#include <MemoryFree.h>         // Mem testing lib for development



////////////////////////////////////////////////////////////////////////////////
// Circuit setup
////////////////////////////////////////////////////////////////////////////////

// ethernet module
#define UIP_CLIENT_TIMER    (-1)            // really set in uipethernet-conf.h
#define HTTP_RETRIES        1               // number of retries if request failes

// nfc readermodule
#define PN532_IRQ           (4)             // spare pin2 for UIP module
#define PN532_RESET         (20)            // not connected by default
#define PN532DEBUG                          // "TIMEOUT!" messages to Serial
//#undef PN532DEBUG
#define NFC_READ_TIMEOUT    (200)           // nfc passive target read timeout

//lcd module
#define LCD_BACKLIGHT_RED   (6)             // red pwm
#define LCD_BACKLIGHT_GREEN (5)             // green pwm
#define LCD_BACKLIGHT_BLUE  (3)             // blue pwm
#define LCD_DATA_RS         (7)             // register select
#define LCD_DATA_EN         (8)             // enable
#define LCD_DATA_D4         (17)            // data bit 4
#define LCD_DATA_D5         (16)
#define LCD_DATA_D6         (15)
#define LCD_DATA_D7         (14)            // ..7

// piezo buzzer
#define BUZZER_PIN          (9)             // piezo buzzer pin

// network configuration
byte netSV[4];                              // server to send request to



////////////////////////////////////////////////////////////////////////////////
// Runtime variables
////////////////////////////////////////////////////////////////////////////////

//eeprom module setup
AH_24Cxx ic_eeprom = AH_24Cxx(EEPROM_EXT_AT24C256, EEPROM_EXT_BUS);

//ethernet module setup
EthernetClient eth;

//nfc module setup
Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

//lcd library setup
//LiquidCrystal lcd(LCD_DATA_RS, LCD_DATA_EN, LCD_DATA_D4, LCD_DATA_D5, LCD_DATA_D6, LCD_DATA_D7);
LiquidCrystal_I2C lcd(0x27,  2, 1, 0, 4, 5, 6, 7, 0, NEGATIVE);
long lcdClockAdj = 0;

// global state variables
byte stateFontSet   = 0;
long stateClockSync = 0;
long stateDownload  = 0;
long stateCardRead  = 0;



////////////////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////////////////
void setup(void) {
  short httpOK;
  short httpCount;
  byte  netMAC[6];                                               // mac address
  byte  netIP[4];                                                // host ip
  byte  netDNS[4];                                               // dns server
  byte  netGW[4];                                                // ip gateway
  byte  i;
  
  //serial port setup
  Serial.begin(115200);
  Serial.println(F("Kartyaolvaso\n  verzio: 1.0\n"));

  //lcd screen setup
  Serial.println(F("LCD beallitas"));
  pinMode(LCD_BACKLIGHT_RED,   OUTPUT);
  pinMode(LCD_BACKLIGHT_GREEN, OUTPUT);
  pinMode(LCD_BACKLIGHT_BLUE,  OUTPUT);
  analogWrite(LCD_BACKLIGHT_RED,   48);
  analogWrite(LCD_BACKLIGHT_GREEN, 0);
  analogWrite(LCD_BACKLIGHT_BLUE,  128);
  Serial.println(F("  Hattervilagitas: ok"));
  lcd.begin(20, 4);
  lcd.clear();
  lcd.noCursor();
  Serial.println(F("  Kepernyotorles: ok\n"));

  //ethernet module setup (vanilla arduino or uip chip)
  Serial.print(F("Halozat\n  MAC address: "));
  for(i=0; i<=5; i++) {
    netMAC[i] = EEPROM.read(EEPROM_INT_MAC + i);
  }
  Serial.println(stringIPQuad(netMAC, 6, ':', HEX));
  Serial.print(F("   IP cim: "));
  for(i=0; i<=3; i++) {
    netIP[i] = EEPROM.read(EEPROM_INT_IP + i);
  }
  Serial.println(stringIPQuad(netIP, 4, '.', DEC));
  Serial.print(F("  DNS cim: "));
  for(i=0; i<=3; i++) {
    netDNS[i] = EEPROM.read(EEPROM_INT_DNS + i);
  }
  Serial.println(stringIPQuad(netDNS, 4, '.', DEC));
  Serial.print(F("   GW cim: "));
  for(i=0; i<=3; i++) {
    netGW[i] = EEPROM.read(EEPROM_INT_GW + i);
  }
  Serial.println(stringIPQuad(netGW, 4, '.', DEC));
  Serial.print(F("   SV cim: "));
  for(i=0; i<=3; i++) {
    netSV[i] = EEPROM.read(EEPROM_INT_SV + i);
  }
  Serial.println(stringIPQuad(netSV, 4, '.', DEC));
  Serial.println();
  Ethernet.begin(netMAC, netIP, netDNS, netGW);
  delay(1000);

  //nfc module setup
  Serial.println(F("NFC modul"));
  nfc.begin();
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.println(F("  Nem talalhato a PN53x modul"));
    //while (1);
  } else {
    Serial.print(F("  Modul verzio: PN5"));
    Serial.println((versiondata>>24) & 0xFF, HEX); 
    Serial.print(F("  Firmware: "));
    Serial.print((versiondata>>16) & 0xFF, DEC); 
    Serial.print('.'); 
    Serial.println((versiondata>>8) & 0xFF, DEC);
    nfc.SAMConfig();
  }
  Serial.println();

  // syncing clock
  httpOK    = -1;
  httpCount = HTTP_RETRIES;
  while(httpOK == -1 && httpCount > 0) {
    httpOK = ethHttpGet(1);
    httpCount--;
  }

  // syncing card db
  httpOK    = -1;
  httpCount = HTTP_RETRIES;
  while(httpOK == -1 && httpCount > 0) {
    httpOK = ethHttpGet(2);
    httpCount--;
  }
  
  // ready
  Serial.println(F("Kesz!\n--------\n")); 
}



////////////////////////////////////////////////////////////////////////////////
// main
////////////////////////////////////////////////////////////////////////////////
void loop(void) {
  nfcRead();
  lcdScreenMain();
  
  
  delay(100);
  //Serial.print("Free mem: "); Serial.println(freeMemory()); Serial.println();
}



////////////////////////////////////////////////////////////////////////////////
// NFC module handling
////////////////////////////////////////////////////////////////////////////////
void nfcRead() {
  uint8_t success;
  uint8_t uid[] = {0, 0, 0, 0, 0, 0, 0};
  uint8_t uidLength;
  int     index;
  uint8_t i;
  uint8_t j;
  String  url = "";

  url.reserve(32);

  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, NFC_READ_TIMEOUT);
  if (success) {
    Serial.println();
    Serial.print(F("Kartya beolvasas: "));
    if (uidLength == 7) {
      uint8_t data[8];  //32 volt!!
      Serial.println(F("NTAG203 kompatibilis kartya"));
      Serial.print  (F("  UID hossz: "));
      Serial.print  (uidLength, DEC);
      Serial.println((" byte"));
      Serial.print  (F("  UID ertek: "));
      Serial.print  (uid[0], HEX); 
      Serial.print  (uid[1], HEX); 
      Serial.print  (uid[2], HEX);
      Serial.print  (uid[3], HEX); 
      Serial.print  (uid[4], HEX); 
      Serial.print  (uid[5], HEX);
      Serial.println(uid[6], HEX); //nfc.PrintHex(uid, uidLength);
      for(i = 5; i < 12; i++) {
        success = nfc.ntag2xx_ReadPage(i, data);
        if (success) {
          Serial.print(F("  Lap "));
          Serial.print(i, DEC);
          Serial.print(F(": "));
          for (uint8_t j = 0; j < 4; j++) {
            Serial.print(data[j], HEX);
            if(isGraph(data[j])) url.concat((char)data[j]);
          }
          Serial.println();
        } else {
          Serial.print(F("  Nem olvashato lap: "));
          Serial.println(i);
          url = "";
          break;
        }
      }
    }
    if(url.length()){
      Serial.print(F("  Beolvasott adat: "));
      Serial.println(url);
      index = stringIndexOf("developpe.club/", url);
      if(index > -1) {
        Serial.print(F("  Kod pozicio: "));
        Serial.println(index + 15);
        Serial.print(F("  Kod: "));
        Serial.println(url.substring(index + 15, index + 21));
        lcdScreenReadOK(url.substring(index + 15, index + 21));
        buzzerAccept();
        delay(2000);
      } else {
        Serial.println(F("  Nincs Developpe berlet info"));
        lcdScreenReadError();
        buzzerDeny();
        delay(2000);
      }
    } else {
      Serial.println(F("  Nem lehet beolvasni az osszes lapot"));
      lcdScreenReadError();
      buzzerError();
      delay(2000);
    }
  }
}



////////////////////////////////////////////////////////////////////////////////
// LCD handling
////////////////////////////////////////////////////////////////////////////////
void lcdScreenMain() {
  byte secs;
  byte mins;
  byte hours;
  String timex = "   ";
  long rsecs = millis()/1000 + lcdClockAdj;

  timex.reserve(20);

  lcd.home();
  lcd.print(F("  Developpe Balett  "));
  analogWrite(LCD_BACKLIGHT_RED,   96);
  analogWrite(LCD_BACKLIGHT_GREEN, 0);
  analogWrite(LCD_BACKLIGHT_BLUE,  255);
  secs  = (rsecs          ) % 60;
  mins  = (rsecs / (  60 )) % 60;
  hours = (rsecs / (3600 )) % 24;
  if(hours<10) timex.concat("0");
  timex.concat(hours);
  timex.concat(":");
  if(mins<10) timex.concat("0");
  timex.concat(mins);
  timex.concat(":");
  if(secs<10) timex.concat("0");
  timex.concat(secs);
  timex.concat("   ");
  lcdLargeFont();
  lcdLargeText(timex, 0, 1);
}

void lcdScreenReadOK(String code) {
  lcd.clear();
  lcd.home();
  lcd.print(F(" Beolvasas ok! "));
  lcd.print(code);
  lcdLargeFont();
  lcdLargeText("r e n d b e n", 0, 1);
  analogWrite(LCD_BACKLIGHT_RED,   255);
  analogWrite(LCD_BACKLIGHT_GREEN, 8);
  analogWrite(LCD_BACKLIGHT_BLUE,  255);
}

void lcdScreenReadError(){
  lcd.clear();
  lcd.print(F("Beolvasas sikertelen!   "));
  lcdLargeFont();
  lcdLargeText("     h i b a    ", 0, 1);
  analogWrite(LCD_BACKLIGHT_RED,   8);
  analogWrite(LCD_BACKLIGHT_GREEN, 255);
  analogWrite(LCD_BACKLIGHT_BLUE,  255);
}

void lcdScreenProgress(String msg) {
  lcd.home();
  lcd.print(msg);
  lcd.setCursor(2,2);
  lcd.print(millis());
}

void lcdLargeText(String text, byte x, byte y) {
  int j = 0;
  for (int i=0; i<text.length(); i++) {
    switch(text.charAt(i)) {
      case '0': lcdWideChar(byte(2), byte(2), byte(3), byte(4), byte(5), byte(6), x+j, y); j+=2; break;
      case '1': lcdWideChar(byte(1), byte(0), ' ',     byte(3), byte(1), byte(5), x+j, y); j+=2; break;
      case '2': lcdWideChar(byte(2), byte(2), byte(2), byte(6), byte(5), byte(2), x+j, y); j+=2; break;
      case '3': lcdWideChar(byte(2), byte(2), byte(1), byte(6), byte(2), byte(6), x+j, y); j+=2; break;
      case '4': lcdWideChar(byte(0), byte(1), byte(5), byte(6), ' ',     byte(4), x+j, y); j+=2; break;
      case '5': lcdWideChar(byte(2), byte(2), byte(5), byte(2), byte(2), byte(6), x+j, y); j+=2; break;
      case '6': lcdWideChar(byte(2), byte(2), byte(5), byte(2), byte(5), byte(6), x+j, y); j+=2; break;
      case '7': lcdWideChar(byte(2), byte(2), ' ',     byte(6), ' ',     byte(4), x+j, y); j+=2; break;
      case '8': lcdWideChar(byte(2), byte(2), byte(5), byte(6), byte(5), byte(6), x+j, y); j+=2; break;
      case '9': lcdWideChar(byte(2), byte(2), byte(5), byte(6), byte(2), byte(6), x+j, y); j+=2; break;
      case 'a': lcdWideChar(byte(2), byte(2), byte(5), byte(6), byte(3), byte(4), x+j, y); j+=2; break;
      case 'b': lcdWideChar(byte(2), byte(0), byte(5), byte(6), byte(5), byte(6), x+j, y); j+=2; break;
      case 'd': lcdWideChar(byte(2), byte(0), byte(3), byte(4), byte(5), byte(6), x+j, y); j+=2; break;
      case 'e': lcdWideChar(byte(2), byte(2), byte(5), byte(0), byte(5), byte(2), x+j, y); j+=2; break;
      case 'h': lcdWideChar(byte(0), byte(1), byte(5), byte(6), byte(3), byte(4), x+j, y); j+=2; break;
      case 'i': lcdWideChar(byte(1), byte(2), ' ',     byte(3), byte(1), byte(5), x+j, y); j+=2; break;
      case 'n': lcdWideChar(byte(2), byte(0), byte(3), byte(4), byte(3), byte(4), x+j, y); j+=2; break;
      case 'r': lcdWideChar(byte(2), byte(0), byte(5), byte(6), byte(3), byte(4), x+j, y); j+=2; break;
      case ' ': lcdNarrowChar(' ',   ' ',     ' ',                                x+j, y); j+=1; break;
      case ':': lcdNarrowChar(' ',   byte(7), byte(7),                            x+j, y); j+=1; break;
      default : lcdWideChar(byte(7), byte(7), byte(7), byte(7), byte(7), byte(7), x+j, y); j+=2; break;
    }   
  }
}

void lcdWideChar(char c0, char c1, char c2, char c3, char c4, char c5, byte x, byte y) {
  lcd.setCursor(x,y);
  lcd.write(c0);
  lcd.write(c1);
  lcd.setCursor(x,y+1);
  lcd.write(c2);
  lcd.write(c3);
  lcd.setCursor(x,y+2);
  lcd.write(c4);
  lcd.write(c5);
}

void lcdNarrowChar(char c0, char c1, char c2, byte x, byte y) {
  lcd.setCursor(x,y);
  lcd.write(c0);
  lcd.setCursor(x,y+1);
  lcd.write(c1);
  lcd.setCursor(x,y+2);
  lcd.write(c2);
}

void lcdLargeFont() {
  byte font[8];
  byte fnum;
  byte row;
  if(stateFontSet != 1) {
    for(fnum=0; fnum<8; fnum++) {
      for(row=0; row<8; row++) {
        font[row] = EEPROM.read(EEPROM_INT_FONT + fnum * 8 + row);
        lcd.createChar(fnum, font);
      }
    }
    stateFontSet = 1;
  }
} 



////////////////////////////////////////////////////////////////////////////////
// HTTP functions
////////////////////////////////////////////////////////////////////////////////
short ethHttpGet(byte type) {
  String url;
  String msg;
  String line = "";
  char    buf = 'x';
  bool loopok = true;
  bool dataok = false;
  
  line.reserve(40);
  
  switch(type) {
    case 1: url = F("/_s/t.php"); msg = F("Ora szinkron");    break;
    case 2: url = F("/_s/d.php"); msg = F("Kartya letoltes"); break;
  }
  
  Serial.println(msg);
  //lcdScreenProgress(msg);
  eth.stop();
  int ret = eth.connect(netSV, 80);
  if (ret == 1) {
    eth.print(F("GET "));
    eth.print(url);
    eth.print(F(" HTTP/1.1\nHost: "));
    eth.println(F("developpe.club"));
    eth.print(F("Connection: close\n\n"));
    while (loopok) {
      if(eth.available()) {
        buf = eth.read();
        if (buf != '\r') {
          if(buf == '\n') {
            if (dataok) {
              switch(type) {
                case 1: ethHttpProcLineClock(line);    break;
                case 2: ethHttpProcLineDownload(line); break;
              }
            }
            if(line.length() == 0) dataok = true;
            line = "";
          } else {
            if(isPrintable(buf) && line.length() < 35) line.concat(buf);
          }
        }
      } else {
        if (!eth.connected()) {
          loopok = false;
        }
      }
    }
    if(line.length() > 0) {
      switch(type){
        case 1: ethHttpProcLineClock(line);    break;
        case 2: ethHttpProcLineDownload(line); break;
      }
    }
  } else {
    Serial.println(F("  kapcsolat sikertelen\n"));
    return -1;
  }
  Serial.println();
  eth.stop();
  return 0;
}

void ethHttpProcLineClock(String line) {
  Serial.print("  ora adatsor: "); Serial.println(line);
  lcdClockAdj = line.toInt();
}

void ethHttpProcLineDownload(String line) {
  Serial.print("  kartya adatsor: "); Serial.println(line);
}


////////////////////////////////////////////////////////////////////////////////
// Piezo buzzer function, using simple/universal phrases
////////////////////////////////////////////////////////////////////////////////
void buzzerAccept() {                       // C5 double-tap / unison
  tone(BUZZER_PIN, 523, 100); 
  delay(120);
  tone(BUZZER_PIN, 523, 100); 
  delay(120);
  noTone(BUZZER_PIN);
}

void buzzerDeny() {                         // D5-A# descending minor third
  tone(BUZZER_PIN, 740, 50); 
  delay(200);
  tone(BUZZER_PIN, 494, 300); 
  delay(300);
  //tone(BUZZER_PIN, 622, 200); delay(200); // interlocked tritone
  //tone(BUZZER_PIN, 466, 300); delay(300);
  noTone(BUZZER_PIN);
}

void buzzerError() {                        // Rapid thrill on major third
  for (byte i=0; i<8; i++) {
    tone(BUZZER_PIN, 659, 30); 
    delay(30);
    tone(BUZZER_PIN, 740, 30); 
    delay(30);
  }
  noTone(BUZZER_PIN);
}



////////////////////////////////////////////////////////////////////////////////
// Utility
////////////////////////////////////////////////////////////////////////////////
int stringIndexOf(String needle, String haystack) {
  int foundpos = -1;
  for (int i = 0; (i < haystack.length() - needle.length()); i++) {
    if (haystack.substring(i,needle.length()+i) == needle) {
      foundpos = i;
    }
  }
  return foundpos;
}

String stringIPQuad(byte ipPtr[], byte n, char s, byte f) {
  String ret = "";
  for(byte i=0; i<n; i++) {
    ret.concat(String(ipPtr[i], f));
    if(i<n-1) ret.concat(s);
  }  
  return ret;
}

